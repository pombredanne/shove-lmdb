'''shove-leveldb fabfile'''

from fabric.api import local


def release():
    local('python setup.py bdist_wheel sdist --format=gztar,bztar,zip upload')

def register():
    local('python setup.py register bdist_wheel sdist --format=gztar,bztar,zip upload')
